package com.qkdversion.OXCDomian;

import android.text.InputFilter.LengthFilter;

/**
 * 定义OXC的配置参数
 * @author Ljj
 *
 */
public final class OXCSetupPara {
	//4*4OXC的IP地址
	public static final String OXC_1_IP="192.168.1.9";
	//波带级8*8OXC的IP地址
	public static final String OXC_2_IP="192.168.1.10"; 
	//波带级8*8OXC1的IP地址
	public static final String OXC_3_IP="192.168.1.13";
	/**
	 * 指令名称：POS
	 * 发送格式：POS  
	 * 功能：查询A->B通道配置情况，
	 * 返回参数格式：POS 1 2 3 4 5 6 7 8 
	 **/
	public final  static String  COMMEND_POS="POS";
	/**
	 * 指令名称：SET
	 * 发送格式：SET x x x x x x x x
	 * 功能：配置A->B通道连接情况
	 * 返回参数格式：SET x x x x x x x x
	 **/
	public final static String COMMEND_SET="SET";
	/**
	 * 指令名称：
	 * 发送格式：SET x x x x x x x x
	 * 功能：配置A->B通道连接情况
	 * 返回参数格式：SET x x x x x x x x
	 */
	/**
	 * 指令名称：IP
	 * 发送格式：IP 无参数用于查询IP配置情况   IP 有参数 用于配置IP
	 * 功能：查询IP或配置IP
	 * 返回参数格式：IP 192.168.10.100/24
	 **/
	public final static String COMMEND_IP="IP";
	/**
	 * 指令名称：GW
	 * 发送格式：GW 无参数用于查询网关配置情况   GW 有参数 用于配置GW 
	 * 功能：查询网关或配置网关
	 * 返回参数格式：GW 192.168.1.1
	 **/
	public final static String COMMEND_GW="GW";
	/**
	 * 若为光纤级交换且目的地为IP1(第一输入通道和第三输出通道对应)
	 **/
	public final static String FIBER_IP1="SET 3 4 1 2";
	/**
	 * 若为光纤级交换且目的地为IP2(第一输入通道和第四输出通道对应)
	 **/
	public final static String FIBER_IP2="SET 4 3 1 2";
	/**
	 * 波带级交换(针对从1输入通道进来的用户)(每个输入通道和每个输出通道相对应)
	 * 4*4
	 **/
	public final static String BAND_1IN="SET 1 2 3 4";
	/**
	 * 波带级交换(针对从1输入通道进来的用户)(每个输入通道和每个输出通道相对应)
	 * 8*8
	 **/
	public final static String BAND_2IN="SET 1 2 3 4 5 6 7 8";
	/** 
	 * 波长级交换(针对从1输入通道进来的用户)(每个输入通道和输出通道相对应)
	 * 4*4
	 **/
	public final static String LENGTH_1IN="SET 1 2 3 4";
	/**
	 * 波长级交换(针对从1输入通道进来的用户)
	 * 8*8
	 * (3进连7出,6进连8出,7进(代表去目的IP1)3出，直接对应光纤级(4*4)从三口进，对应从3口出，即可到达目的IP1)
	 **/
	public final static String LENGTH_2IN="SET 1 2 7 4 5 8 3 6";
	 /** 
	  * 波长级交换(针对从1输入通道进来的用户)
	  * 8*8
	  **/
	public final static String LENGTH_3IN="SET 1 2 3 4 5 6 7 8";
	
}
