package com.qkdversion.msgDomain;

import android.R.string;

public class MsgConstant {
	//光纤级交换
	public final static int FIBER=0;
	//波带级交换
	public final static int WAVEBAND=1;
	//波长级交换
	public final static int WAVELENGTH=2;
	//目的IP1
	public final static String  IP1="10.109.0.1";
	//目的IP2
	public final static String IP2="10.109.0.2"; 

}
