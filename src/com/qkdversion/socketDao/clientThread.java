package com.qkdversion.socketDao;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import android.R.integer;
import android.text.InputFilter.LengthFilter;
import android.util.Log;

import com.qkdversion.OXCDomian.OXCAckPara;
import com.qkdversion.OXCDomian.OXCSetupPara;
import com.qkdversion.msgDao.MsgDao;
import com.qkdversion.msgDomain.Ack;
import com.qkdversion.msgDomain.Msg;
import com.qkdversion.msgDomain.MsgConstant;
import com.qkdversion.oxcDao.OXCSetupCl;
import com.qkdversion.oxcService.OXCService;
import com.qkdversion.socketDomain.ServerPara;
import com.qkdversion.wssDao.WSSSetupCl;
import com.topeet.serialtest.Com;
import com.topeet.serialtest.serial;
/**
 * 描述:主要逻辑在此线程中执行，相当于主函数
 * @author Ljj 2015-04-24
 */
public class clientThread implements Runnable {
	static {
		System.loadLibrary("serialtest");
	}
	private static final String TAG = "clientThread";
	private serial com1;//第一个WSS
	private serial com2;//第二个WSS
	private Socket s;
	private boolean isFinish = false;// 用于判断一次分配是否完成	
	private Msg msg;// 用于保存消息
	private String str;// 用于接收消息	
	private OXCSetupCl oxc_1;//telnet控制器 oxc 4*4(光纤)
	private OXCSetupCl oxc_2;//telnet控制器 oxc 8*8(波带)
	private OXCSetupCl oxc_3;//telnet控制器 oxc 8*8(波长)
	@Override
	public void run() {   
		try {
			init();//初始化
			Log.i(TAG, "Thread" + Thread.currentThread().getName());
	        int initState= checkDevice();// 检查设备状况
			s = new Socket(ServerPara.Server_IP, ServerPara.Server_Port);//连接服务器
			writeToServer(initState+"");	// 将设备状况向服务器反馈
			while (true) {
				str = readFromServer();// 等待服务器数据包,若无数据，会一直阻塞
				if (str != null && str != "") {
					Log.i(TAG, "get str from server" + str);
					msg = MsgDao.getInstance().getMSg(str);// 将接收到的字符串信息封装成Message
				//	writeToServer(Ack.MESSAGE_OK+"");   //向服务器反馈数据接收ok
					isFinish = true;
				}
				if (isFinish) {// 利用消息配置整个设备
					isFinish = false;
					int resAck = messageDispatcher(msg);	// 进行数据包解析和数据分配
					Log.i(TAG,"分配结果值:"+resAck);
						writeToServer(resAck+"");
					str = null;// 将接收字符串数据置空
				}
			}
		} catch (UnknownHostException e) {
			e.printStackTrace(); 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 初始化函数
	 **/
	 private void init(){
	    com1= new serial();
		com1.Open(3, 115200,Com.WSS1);
		com2 = new serial();
	    com2.Open(4, 115200,Com.WSS2);
	    oxc_1=new OXCSetupCl();
		oxc_2=new OXCSetupCl();
		oxc_3=new OXCSetupCl();
	 }
	/**
	 * 从服务器读取信息
	 **/
	private String readFromServer() {
		String str = "";
		InputStream is = null;
		try {
			if (s != null) {
				is = s.getInputStream();
				byte[] bytes = new byte[1024];
				int len = 0;
				len = is.read(bytes);
				if (len != -1) {
					str = new String(bytes, 0, len, "utf-8");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return str;
	}
	/**
	 * 向服务器返回信息
	 **/
	private void writeToServer(String ack) {
		OutputStream os = null;
		try {
			if (s != null) {
				os = s.getOutputStream();
				os.write(ack.getBytes("utf-8"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 检查设备状况
	 **/
	private int checkDevice() {
		int device_flag=0;
		// 检查光纤级OXC开关
		int isConnect_1=oxc_1.connectToTelnet(OXCSetupPara.OXC_1_IP);
		if(OXCAckPara.CONNECT_FAIL==isConnect_1){
			device_flag=Ack.DEVICE_1_ERR;
			return device_flag;
		}
		// 检查波带级光开关
		int isConnect_2=oxc_2.connectToTelnet(OXCSetupPara.OXC_2_IP);
		if(OXCAckPara.CONNECT_FAIL==isConnect_2){
			device_flag=Ack.DEVICE_2_ERR;
			return device_flag;
		}
		// 检查波长级光开关
		int isConnect_3=oxc_3.connectToTelnet(OXCSetupPara.OXC_3_IP);
		if(OXCAckPara.CONNECT_FAIL==isConnect_3){
			device_flag=Ack.DEVICE_4_ERR;
			return device_flag;
		}
		// 检查WSS1
		if (!WSSSetupCl.getInstance().checkWSS(com1,Com.WSS1)) {
			device_flag=Ack.DEVICE_3_ERR;
			return device_flag;
		}
		// 检查WSS2
		if (!WSSSetupCl.getInstance().checkWSS(com2,Com.WSS2)) { 
			device_flag=Ack.DEVICE_5_ERR;
			return device_flag;
		}
		device_flag=Ack.DEVICE_OK;
		return device_flag;
	}
	
	/**
	 * 进行消息处理 
	 **/
	private int messageDispatcher(Msg msg) {
		int resFlag =0;	// 用于指示状态，一旦某个环节出错，立即返回对应的信息
		int switchType=msg.getSwitchType();
		switch(switchType){
		case MsgConstant.FIBER://光纤级交换
			 resFlag=fiberSwitch(msg,oxc_1);//进行光线级交换
			break;
		case MsgConstant.WAVEBAND://波带级交换
			resFlag=bandSwitch(msg,oxc_1,oxc_2,com1);
			break;
		case MsgConstant.WAVELENGTH://波长级交换
			resFlag=LengthSwitch(); 
			break;
		}
		return resFlag;
	}
	/**
	 * 光纤级交换
	 **/
	private int fiberSwitch(Msg msg,OXCSetupCl oxcCl) {
		int fiberAck=0;
			// 取出目的IP
			if (MsgConstant.IP1.equals(msg.getFiberIp())) {
				String command =OXCSetupPara.FIBER_IP1;// 目的IP和IP1相等，OXC4*4中通道1-----通道3,指令为set 3 x x x
				fiberAck=OXCService.getOXCService().oxcSwitch(oxcCl,OXCSetupPara.OXC_1_IP,command);
				Log.i(TAG,"光纤级交换目的地为IP1"+fiberAck);
				return fiberAck;
			} else if (MsgConstant.IP2.equals(msg.getFiberIp())) {
				String command =OXCSetupPara.FIBER_IP2;// 目的IP和IP1相等，OXC4*4中通道1-----通道4,指令为set 4 x x x
				fiberAck=OXCService.getOXCService().oxcSwitch(oxcCl,OXCSetupPara.OXC_1_IP,command);
				Log.i(TAG,"光纤级交换目的地为IP2"+fiberAck);
                return fiberAck;
			}
		fiberAck=Ack.FIBER_OK;
		return fiberAck;
	}
	/**
	 * 波带级交换
	 */
	private int bandSwitch(Msg msg, OXCSetupCl oxc_1,OXCSetupCl oxc_2, serial com) {
		int bandAck=0;
		//配置4*4
		String command =OXCSetupPara.BAND_1IN;//每个通道和每个通道相对应
		bandAck=OXCService.getOXCService().oxcSwitch(oxc_1,OXCSetupPara.OXC_1_IP,command);
		if(Ack.OXCSETUP_1_OK!=bandAck)
			return bandAck;
		Log.i(TAG,"波带级交换4*4配置结果"+bandAck);
		//配置WSS
		bandAck= WSSSetupCl.getInstance().setWssPort(msg, com1,Com.WSS1);
		if(Ack.WSSSENDDATA_OK!=bandAck){//配置失败，直接将信息返回去 
			return bandAck;
		}
		Log.i(TAG,"Wss配置结果"+bandAck);
		//配置波带级8*8
		command=OXCSetupPara.BAND_2IN;
		bandAck=OXCService.getOXCService().oxcSwitch(oxc_2,OXCSetupPara.OXC_2_IP,command);
		if(Ack.OXCSETUP_2_OK!=bandAck)
			return bandAck;
		Log.i(TAG,"波带级交换8*8配置结果"+bandAck);
		bandAck=Ack.BAND_OK;
		return bandAck;
	}
    /**
     * 波长级交换
     */
	private int LengthSwitch() {
	    int lengthAck=0;
	    //配置4*4
	    String command =OXCSetupPara.LENGTH_1IN;
	    lengthAck=OXCService.getOXCService().oxcSwitch(oxc_1,OXCSetupPara.OXC_1_IP, command);
	    if(Ack.OXCSETUP_1_OK!=lengthAck)
	    	return lengthAck;
	    Log.i(TAG,"波长级交换4*4配置结果"+lengthAck);
	    //配置WSS
	    lengthAck= WSSSetupCl.getInstance().setWssPort(msg, com2,Com.WSS2);
	  	if(Ack.WSSSENDDATA_OK!=lengthAck){//配置失败，直接将信息返回去 
	  		return lengthAck;  
	  	}
	    //配置波带级8*8
	  	command=OXCSetupPara.LENGTH_2IN;
	  	lengthAck=OXCService.getOXCService().oxcSwitch(oxc_2,OXCSetupPara.OXC_2_IP,command);
	  	if(Ack.OXCSETUP_2_OK!=lengthAck)
	  	 return lengthAck;
	  	Log.i(TAG,"波带级交换8*8配置结果"+lengthAck);
	   //配置波长级8*8
	  	command=OXCSetupPara.LENGTH_3IN;
	  	lengthAck=OXCService.getOXCService().oxcSwitch(oxc_3,OXCSetupPara.OXC_3_IP,command);
	  	if(Ack.OXCSETUP_3_OK!=lengthAck)
		  	return lengthAck;
		Log.i(TAG,"波长级交换8*8配置结果"+lengthAck);
		lengthAck=Ack.LEGNTH_OK;
		return lengthAck;
	}
}
