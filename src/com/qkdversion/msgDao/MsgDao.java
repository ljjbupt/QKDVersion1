package com.qkdversion.msgDao;
//消息范例："1$0$3$10.108.0.1,1566.723,1566.314,1565.905,10.109.0.1,0$10.108.0.2,1565.496,1565.087,1564.679,10.109.0.2,0$+
//10.108.0.3,1564.271,1563.863,1563.455,10.109.0.1,10.109.0.2$&";
import java.util.List;
import com.qkdversion.msgDomain.Msg;
import com.qkdversion.msgDomain.UserMsg;
/**
 * 类型：msg的控制层
 * @author Ljj
 *
 */
public final class MsgDao {
	private static MsgDao instance = new MsgDao();

	private MsgDao() {
	}

	public static MsgDao getInstance() {
		return instance;
	}
    /**
     *  将接收到的字符串转换成Msg
     **/
	public Msg getMSg(String str) {
		Msg msg = new Msg();
		String[] strs = str.split("\\$");
		msg.setSwitchType(Integer.valueOf(strs[0]));
		msg.setFiberIp(strs[1]);
		msg.setUserNum(Integer.valueOf(strs[2]));
		List<UserMsg> userList = msg.getUserMsg();
		int Usernum = msg.getUserNum();
		for (int i = 3; i < 3 + Usernum; i++) {
			userList.add(setUserMsg(strs[i]));
		}
		msg.setEOF(strs[3 + Usernum]);
		return msg;
	}
	/**
     *  设置用户信息
     **/
	private UserMsg setUserMsg(String str) {
		String[] userStrs = str.split(",");
		UserMsg userMsg = new UserMsg();
		userMsg.setIpSource(userStrs[0]);
		userMsg.setSynWave(Double.valueOf(userStrs[1]));
		userMsg.setQuanWave(Double.valueOf(userStrs[2]));
		userMsg.setClassWave(Double.valueOf(userStrs[3]));
		userMsg.setIpDes1(userStrs[4]);
		userMsg.setIpDes2(userStrs[5]);
		return userMsg;
	}
}
